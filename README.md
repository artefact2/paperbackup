paperbackup
===========

My own take on a paper backup solution. Released under the Apache
License, version 2.0.

Heavily “inspired” by intra2net's version:
https://github.com/intra2net/paperbackup

Most settings can be customised: paper size, font size, code size,
chunk size (bytes per code).

**See a demo here: [demo.pdf](./demo.pdf). Should you successfully
  restore that data, you should recognise a well-known test image.**

Dependencies
============

* PHP ≥ 7.2
* dmtx-utils (`dmtxread`, `dmtxwrite`)
* Inkscape
* poppler (`pdfunite`, `pdftoppm`)
* parallel

Usage
=====

Generate DataMatrix codes:

~~~
./gen-paper-dmtx codes-foo.pdf < foo
~~~

Generate a line-by-line listing:

~~~
./gen-paper-listing listing-foo.pdf < foo
~~~

Restore data from scanned DataMatrix codes:

~~~
./restore-dmtx SCAN*.JPG > restored-foo
~~~

Restore data when some codes couldn't be automatically decoded:

~~~
./restore-dmtx-raw SCAN*.JPG | sort > restored-foo.txt
# Check restored-foo.txt for missing code numbers, fill these by hand
# either by scanning manually or by reading the matching line in the
# listing.
cut -d' ' -f2 restored-foo.txt | base64 -d > restored-foo

# Check file integrity compared to the printed hash in the listing:
sha3-512sum -b restored-foo | base64
~~~
